List of dependencies and what they do:

cookie-parser: ~1.4.3

-

debug: ~2.6.9

-

eslint: ^5.6.0

-

express: ~4.16.0

-

express-mysql-session: ^2.0.1

-

http-errors: ~1.6.2

-

morgan: ~1.9.0

-

mysql: ^2.16.0

-

npm: ^6.4.1

-

path: ^0.12.7

-

promise-mysql: ^3.3.1

-

pug: 2.0.0-beta11

-

session: ^0.1.0