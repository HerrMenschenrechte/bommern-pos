const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser')

router.use(express.json())
router.use(bodyParser.urlencoded({extended: true}))
router.use(bodyParser.json())

var products = require('../models/products')


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('main', { title: 'SV Bommern POS' });
});

router.get('/customer', function(req, res, next) {
  res.render('customer', { title: 'Customer Menu' });
});

router.get('/cart', function(req, res, next) {
  res.render('cart', { title: 'Shopping Cart' });
});

router.get('/confirmation', function(req, res, next) {
  res.render('confirmation', { title: 'Thank you!' });
});

router.post('/purchase', function(req, res, next) {
  
 products.orderFromCart(req);
 res.end("The cart data has been stored");

});


router.get('/booking', function(req, res, next) {
    global.dbh.query("SELECT * FROM test_db.orders").then(function(rows) {
    console.log(rows)
    res.render('booking', {title: 'Bookings'})
  })


});

router.get('/bookingdata', function(req, res, next) {
  global.dbh.query("SELECT * FROM test_db.orders").then(function(rows) {
  console.log(rows)
  res.send(rows)
})


});

module.exports = router;