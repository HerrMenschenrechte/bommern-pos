var mysql = require('promise-mysql')

var dbPool

function createPool () {
  if (!dbPool) {
    dbPool = mysql.createPool({
      connectionLimit: 10,
      port: 3306,
      host: 'localhost',
      user: 'root',
      password: 'password',
      database: 'test_db',
    })
   
    //console.log(`Connected to the database test_db`)
    // console.log(dbPool)
    // dbPool.query('select `product_name` from orders').then(function(rows) {
    // console.log(rows)
    // })

  }
  return dbPool
}


module.exports = createPool()
