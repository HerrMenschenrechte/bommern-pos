



var result = []

$.ajax({
    url: "/bookingdata",
    type: "GET",
    success: function(data) {
        console.log("Data transfer successful. This is the data: " + JSON.stringify(data))
        result.push(data)
        $('#bookings').DataTable({
            data: result[0],
            columns: [
                {data: "order_id"},
                {data: "product_name"},
                {data: "product_price"},
                {data: "product_id"}
            ]
        });
        console.log(result)
    },
    error: function() {
        console.log("Error occurs here")
    },

})


$(document).ready(function() {
    
    $('#bookings1').DataTable()
} );