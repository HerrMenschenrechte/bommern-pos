// variables that hold the prices for the products
// variable total price holds the sum of all items that have been added to the shopping cart
// items have the id "item"Pick in frontend
var totalPrice = 0
var numberOfItems = 0

const product = {

   "drinks": {
        "coffee": { "name": "Coffee", "price": 2, "id": 1},
        "beer": { "name": "Beer", "price": 3, "id": 2},
        "cola": { "name": "Cola", "price": 2, "id": 3},
        "cocktail": { "name": "Cocktail", "price": 5, "id": 4}    
    },

    "food": {
        "muffin":{"name": "Muffin", "price": 1, "id": 5},
        "pizza": { "name": "Pizza", "price": 2.5, "id": 6},
        "cake": { "name": "Cake", "price": 2, "id": 7},
        "ice": { "name": "Ice Cream", "price": 0.6, "id": 8}

    }
}

 var shoppingCart = []; 


// function that calculates the current value of the items in the shopping cart



// functions that add a product to the shopping cart (data validation in frontend)
// products are currently added simply by changing the html code
// add data validation later on for security (e.g. customers can only order x-amount of a certain item)

$('#coffeePick').click(function() {
    totalPrice += product.drinks.coffee.price;
    numberOfItems += 1;
    
    shoppingCart.push(product.drinks.coffee);
    
    console.log(totalPrice);
    console.log(numberOfItems);
    console.log(shoppingCart);
        
    document.getElementById('count').innerHTML = numberOfItems + " x";
    document.getElementById('price').innerHTML = totalPrice.toFixed(2) + " €";
})

$('#beerPick').click(function() {
    totalPrice += product.drinks.beer.price
    numberOfItems += 1

    console.log(totalPrice)
    shoppingCart.push(product.drinks.beer)
    console.log(shoppingCart)



    document.getElementById('count').innerHTML = numberOfItems + " x"
    document.getElementById('price').innerHTML = totalPrice.toFixed(2) + " €"
})

$('#colaPick').click(function() {
    totalPrice += product.drinks.cola.price
    numberOfItems += 1

    console.log(totalPrice)
    shoppingCart.push(product.drinks.cola)
    console.log(shoppingCart)


    document.getElementById('count').innerHTML = numberOfItems + " x"
    document.getElementById('price').innerHTML = totalPrice.toFixed(2) + " €"
})

$('#cocktailPick').click(function() {
    totalPrice += product.drinks.cocktail.price
    numberOfItems += 1

    console.log(totalPrice)
    shoppingCart.push(product.drinks.cocktail)
    console.log(shoppingCart)

    document.getElementById('count').innerHTML = numberOfItems + " x"
    document.getElementById('price').innerHTML = totalPrice.toFixed(2) + " €"
})

$('#cakePick').click(function() {
    totalPrice += product.food.cake.price
    numberOfItems += 1

    console.log(totalPrice)
    shoppingCart.push(product.food.cake)
    console.log(shoppingCart)

    document.getElementById('count').innerHTML = numberOfItems + " x"
    document.getElementById('price').innerHTML = totalPrice.toFixed(2) + " €"
})

$('#muffinPick').click(function() {
    totalPrice += product.food.muffin.price
    numberOfItems += 1

    console.log(totalPrice)
    shoppingCart.push(product.food.muffin)

    document.getElementById('count').innerHTML = numberOfItems + " x"
    document.getElementById('price').innerHTML = totalPrice.toFixed(2) + " €"
})

$('#icecreamPick').click(function() {
    totalPrice += product.food.ice.price
    numberOfItems += 1

    console.log(totalPrice)
    shoppingCart.push(product.food.ice)
    console.log(shoppingCart)


    document.getElementById('count').innerHTML = numberOfItems + " x"
    document.getElementById('price').innerHTML = totalPrice.toFixed(2) + " €"
})

$('#pizzaPick').click(function() {
    totalPrice += product.food.pizza.price
    numberOfItems += 1

    console.log(totalPrice)
    shoppingCart.push(product.food.pizza)
    console.log(shoppingCart)

    document.getElementById('count').innerHTML = numberOfItems + " x"
    document.getElementById('price').innerHTML = totalPrice.toFixed(2) + " €"
})

$('#clearCart').click(function() {
    totalPrice = 0
    numberOfItems = 0

    console.log(totalPrice)
    shoppingCart = []
    console.log(shoppingCart)

    document.getElementById('count').innerHTML = numberOfItems + " x"
    document.getElementById('price').innerHTML = totalPrice.toFixed(2) + " €"
})


$('#purchase').on("click", function() {
    $.ajax({
        url: "/purchase",
        type: "POST",
        data: {shoppingCart},
        success: function(data) {
            console.log("Data transfer successful. This is the data: " + data)
        },
        error: function() {
            console.log("Error occurs here")
        },

    })
    totalPrice = 0
    numberOfItems = 0
    document.getElementById('count').innerHTML = numberOfItems + " x"
    document.getElementById('price').innerHTML = totalPrice.toFixed(2) + " €"
    shoppingCart = []
})