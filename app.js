const express = require('express')
const createError = require('http-errors')
const path = require('path')
const expressValidator = require('express-validator')
const bodyParser = require('body-parser')

// app instance

const app = express()

// db connection

global.dbh = require('./database')

// view engine setup

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')


app.use(express.json())
app.use(express.urlencoded({ extended: false }))

// public files
app.use(express.static(path.join(__dirname, 'public')))

const indexRouter = require('./routes/index')

app.use('/', indexRouter)

app.use(expressValidator())

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
})


app.listen(3000, function(){
  console.log("info",'Server is running at port : ' + 3000);
});


module.exports = app
